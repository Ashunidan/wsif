# WSI-v2
Address: http://localhost:8080
Endpoints:
    * GET /students ? firstname & lastname & birthdate & direction                          - lista studentów, parametry wyszukiwania
        * GET /{index}                                                                      - pojedynczy student po indeksie
        * POST / {index, firstName, lastName, birthDate}                                    - dodanie studenta
        * PUT /{index} {index, firstName, lastName, birthDate}                              - update studenta
        * DELETE /{index}                                                                   - usunięcie studenta
        * GET /{index}/grades ? note & noteDirection & date & dateDirectiom & courseId      - pobranie ocen studenta według parametrów
        * GET /{index}/courses                                                              - pobranie przedmiotów studenta (jeśli ma ocenę)
        * POST /{index}/grades {note, date, courseId}                                       - dodanie oceny dla studenta
        * PUT /{index}/grades/{id} {note, date, courseId}                                   - update oceny

    * GET /courses ? lecturer & name                                                        - lista przedmiotów + parametry
        * GET /{id}                                                                         - pojedynczy kurs po indeksie
        * POST / {name, lecturer}                                                           - dodanie przedmiotu
        * PUT /{id} {name, lecturer}                                                        - update przedmiotu
        * DELETE /{id}                                                                      - usunięcie przedmiotu
        * GET /{id}/grades ? note & noteDirection & date & dateDirection                    - pobranie ocen z przedmiotu

    * GET /grades ? note & noteDirection & date & dateDirection & studentIndex & courseId   - lista ocen + parametry
        * GET /{id}                                                                         - pojedyncza ocena po id
        * DELETE /{id}                                                                      - usunięcie oceny po id