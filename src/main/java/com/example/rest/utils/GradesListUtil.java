package com.example.rest.utils;

import com.example.rest.models.Grade;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Filip Winiarz
 */
public abstract class GradesListUtil {
    public static List<Grade> getGradesByNote(List<Grade> grades, double note, DirectionEnum direction) {
        List<Grade> result = grades;
        switch(direction) {
            case LESSER:
                result = result.stream().filter(grade -> grade.getNote() < note).collect(Collectors.toList());
                break;
            case EQUAL:
                result = result.stream().filter(grade -> grade.getNote() == note).collect(Collectors.toList());
                break;
            case GREATER:
                result = result.stream().filter(grade -> grade.getNote() > note).collect(Collectors.toList());
                break;
            default:
                break;
        }

        return result;
    }

    public static List<Grade> getGradesByDate(List<Grade> grades, Date date, DirectionEnum direction) {
        List<Grade> result = grades;
        switch (direction) {
            case LESSER:
                result = result.stream().filter(grade -> grade.getDate().before(date)).collect(Collectors.toList());
                break;
            case EQUAL:
                result = result.stream().filter(grade -> grade.getDate().equals(date)).collect(Collectors.toList());
                break;
            case GREATER:
                result = result.stream().filter(grade -> grade.getDate().after(date)).collect(Collectors.toList());
                break;
        }

        return result;
    }
}
