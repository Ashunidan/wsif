package com.example.rest.utils;

import com.example.rest.models.Counter;
import com.example.rest.models.Course;
import com.example.rest.models.Grade;
import com.example.rest.models.Student;
import org.mongodb.morphia.Datastore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Filip Winiarz
 */
public class InitialData {
    private static List<Student> students = new ArrayList<>();
    private static List<Course> courses = new ArrayList<>();
    private static List<Grade> grades = new ArrayList<>();
    private static List<Counter> counters = new ArrayList<>();

    public static void initializeData() throws ParseException {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();

        if(datastore.getCount(Counter.class) == 0) {
            initializeCounters(datastore);
        }

        if(datastore.getCount(Student.class) == 0) {
            initializeStudents(datastore);
        }

        if(datastore.getCount(Course.class) == 0) {
            initializeCourses(datastore);
            initializeGrades(datastore);
        }
    }

    private static void initializeCounters(Datastore datastore) {
        counters.add(new Counter("courseId"));
        counters.add(new Counter("gradeId"));
        datastore.save(counters);
    }

    private static void initializeStudents(Datastore datastore) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        students.add(new Student(111222, "Filip", "Winiarz", formatter.parse("1993-03-24")));
        students.add(new Student(222333, "Marian", "Paździoch", formatter.parse("1990-01-01")));
        students.add(new Student(333444, "Donald", "Trump", formatter.parse("1994-09-13")));
        datastore.save(students);
    }

    private static void initializeCourses(Datastore datastore) {
        courses.add(new Course("COURSE_1", "Barrack Obama"));
        courses.add(new Course("COURSE_2", "Kim Dzong Un"));
        courses.add(new Course("COURSE_3", "Wiktor Orban"));
        datastore.save(courses);
    }

    private static void initializeGrades(Datastore datastore) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Grade g1 = new Grade(5.0, dateFormat.parse("2018-04-22"), students.get(0), courses.get(0).getCourseId());
        Grade g2 = new Grade(4.5, dateFormat.parse("2018-04-29"), students.get(0), courses.get(0).getCourseId());
        Grade g3 = new Grade(2.0, dateFormat.parse("2018-04-15"), students.get(1), courses.get(0).getCourseId());
        Grade g4 = new Grade(5.0, dateFormat.parse("2018-04-20"), students.get(0), courses.get(1).getCourseId());
        Grade g5 = new Grade(3.0, dateFormat.parse("2018-04-15"), students.get(2), courses.get(2).getCourseId());
        datastore.update(courses.get(0), datastore.createUpdateOperations(Course.class).add("grades", g1));
        datastore.update(courses.get(0), datastore.createUpdateOperations(Course.class).add("grades", g2));
        datastore.update(courses.get(0), datastore.createUpdateOperations(Course.class).add("grades", g3));
        datastore.update(courses.get(1), datastore.createUpdateOperations(Course.class).add("grades", g4));
        datastore.update(courses.get(2), datastore.createUpdateOperations(Course.class).add("grades", g5));
//
//        grades.addAll(Arrays.asList(g1, g2, g3, g4, g5));
//        datastore.save(grades);
    }
}
