package com.example.rest.utils;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

/**
 * Created by Filip Winiarz
 */
public class WSIDatastore {
    private static final int MONGO_PORT = 8004;

    private static WSIDatastore Instance = new WSIDatastore();
    private Datastore datastore;

    public static WSIDatastore getInstance() {
        return Instance;
    }

    private WSIDatastore() {
        final Morphia morphia = new Morphia();
        datastore = morphia.createDatastore(new MongoClient("localhost", MONGO_PORT), "wsi-mongo1");
        morphia.mapPackage("com.example.rest.models");
        datastore.ensureIndexes();
    }

    public Datastore getDatastore() {
        return datastore;
    }
}
