package com.example.rest.finders;

import com.example.rest.models.Course;
import com.example.rest.models.Grade;
import com.example.rest.models.Student;
import com.example.rest.utils.DirectionEnum;
import com.example.rest.utils.GradesListUtil;
import org.mongodb.morphia.Datastore;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class GradeManager {

    private Datastore datastore;

    public GradeManager(Datastore datastore) {
        this.datastore = datastore;
    }

    public List<Grade> findGrades(Double note, DirectionEnum noteDir, Date date, DirectionEnum dateDir, Long studentIndex, Long courseId) {
        List<Course> courses = new CourseManager(datastore).findCourses(null, null);
        List<Grade> grades = courses.stream().flatMap(course -> course.getGrades().stream()).collect(Collectors.toList());

        if(note != null)
            grades = GradesListUtil.getGradesByNote(grades, note, noteDir);

        if(date != null)
            grades = GradesListUtil.getGradesByDate(grades, date, dateDir);

        if(studentIndex != null)
            grades = grades.stream().filter(grade -> studentIndex.equals(grade.getStudent().getIndex())).collect(Collectors.toList());

        if(courseId != null)
            grades = grades.stream().filter(grade -> courseId.equals(grade.getCourseId())).collect(Collectors.toList());

        return grades;
    }

    public Grade findGradeById(long id) {
//        Grade result = datastore.find(Grade.class).field("id").equal(id).get();
//        return result;
        Grade grade = null;
        List<Course> courses = new CourseManager(datastore).findCourses(null, null);
        if(courses != null) {
            List<Grade> foundGrades = courses.stream()
                    .flatMap(course -> course.getGrades().stream())
                    .filter(courseGrade -> id == courseGrade.getId())
                    .collect(Collectors.toList());

            if(foundGrades != null & foundGrades.size() > 0) {
                grade = foundGrades.get(0);
            }
        }

        return grade;
    }

    public Grade saveGrade(long studentIndex, Grade grade) {
        Student student = datastore.find(Student.class).field("index").equal(studentIndex).get();
        Course course = datastore.find(Course.class).field("courseId").equal(grade.getCourseId()).get();

        if(student == null || course == null)
            return null;

        grade.initId();
        grade.setStudent(student);
        course.getGrades().add(grade);

        datastore.save(grade);
        datastore.save(course);
        return grade;
    }

    public Grade updateGrade(long studentIndex, long gradeId, Grade newGrade) {
        Student student = new StudentManager(datastore).findStudentByIndex(studentIndex);
        Grade grade = findGradeById(gradeId);
        if(student == null || grade == null)
            return null;

        Course course = new CourseManager(datastore).findCourseById(newGrade.getCourseId());
        if(course == null)
            return null;

        newGrade.setStudent(student);
        newGrade.setId(grade.getId());

        int gradeIndex = course.getGrades().indexOf(newGrade);
        if(gradeIndex != -1) {
            course.getGrades().get(gradeIndex).updateData(newGrade);
            datastore.save(course);
        }

        datastore.save(newGrade);
        return newGrade;
    }

    public boolean deleteGrade(long gradeId) {
        Grade toDelete = findGradeById(gradeId);
        List<Course> courses = new CourseManager(datastore).findCourses(null, null);

        if(toDelete == null)
            return false;

        if(courses != null) {
            for(Course course : courses) {
                boolean deleted = course.getGrades().remove(toDelete);
                if(deleted) {
                    datastore.save(course);
                    break;
                }
            }
        }

        try {
            datastore.delete(toDelete);
        } catch (Exception e) {
            System.out.print("");
        }
        return true;
    }
}
