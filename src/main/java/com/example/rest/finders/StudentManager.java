package com.example.rest.finders;

import com.example.rest.models.Course;
import com.example.rest.models.Grade;
import com.example.rest.models.Student;
import com.example.rest.utils.DirectionEnum;
import com.example.rest.utils.GradesListUtil;
import com.mongodb.DuplicateKeyException;
import org.mongodb.morphia.Datastore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class StudentManager {

    private Datastore datastore;

    public StudentManager(Datastore datastore) {
        this.datastore = datastore;
    }

    public List<Student> findStudents(String firstNameQuery, String lastNameQuery, Date birthDateQuery, DirectionEnum birthDateDirection, Long indexQuery) {
        List<Student> students = datastore.find(Student.class).asList();

        if(indexQuery != null) {
            students = students.stream().filter(student -> indexQuery.equals(student.getIndex())).collect(Collectors.toList());
        }

        if(firstNameQuery != null) {
            students = students.stream().filter(student -> student.getFirstName().equals(firstNameQuery)).
                    collect(Collectors.toList());
        }
        if(lastNameQuery != null) {
            students = students.stream().filter(student -> student.getLastName().equals(lastNameQuery)).
                    collect(Collectors.toList());
        }

        if(birthDateQuery != null) {
            switch(birthDateDirection) {
                case LESSER:
                    students = students.stream().filter(student -> student.getBirthDate().before(birthDateQuery))
                            .collect(Collectors.toList());
                    break;
                case EQUAL:
                    students = students.stream().filter(student -> student.getBirthDate().equals(birthDateQuery))
                            .collect(Collectors.toList());
                    break;
                case GREATER:
                    students = students.stream().filter(student -> student.getBirthDate().after(birthDateQuery))
                            .collect(Collectors.toList());
                    break;
                default:
                    break;
            }
        }

        return students;
    }

    public Student findStudentByIndex(Long index) {
        Student result = datastore.find(Student.class).field("index").equal(index).get();
        return result;
    }

    public Student saveStudent(Student student) throws DuplicateKeyException {
        datastore.ensureIndexes();
        datastore.save(student);

        return student;
    }

    public Student updateStudent(Long index, Student student) {
        Student updatedStudent = findStudentByIndex(index);

        if(updatedStudent == null) {
            return null;
        } else {
            student.setId(updatedStudent.getId());
        }

        datastore.save(student);
        return student;
    }

    public boolean cascadeDeleteStudent(Long index) {
        Student student = findStudentByIndex(index);

        if(student == null)
            return false;

        List<Course> courses = datastore.find(Course.class).asList();
        for(Course course: courses) {
            course.getGrades().removeAll(course.getStudentGradesList(index));
            datastore.save(course);
        }
        datastore.delete(student);

        return true;
    }

    public List<Grade> findStudentGrades(long studentIndex, Double note, Long courseId, DirectionEnum noteDirection, Date date,
                                         DirectionEnum dateDirection) {
        List<Course> courses = datastore.find(Course.class).asList();

        if(courseId != null) {
            courses = courses.stream().filter(it -> courseId.equals(it.getCourseId())).collect(Collectors.toList());
        }

        List<Grade> grades = courses.stream().
                flatMap(course -> course.getStudentGradesList(studentIndex).stream()).collect(Collectors.toList());

        if(note != null) {
            grades = GradesListUtil.getGradesByNote(grades, note, noteDirection);
        }

        if(date != null) {
            grades = GradesListUtil.getGradesByDate(grades, date, dateDirection);
        }

        return grades;
    }

    public List<Course> findStudentCourses(long studentIndex) {
        List<Course> studentCourses = new ArrayList<>();
        List<Course> courses = datastore.find(Course.class).asList();
        courses.forEach(course -> extractStudentCourseFromGrades(studentIndex, studentCourses, course));

        return studentCourses;
    }

    private void extractStudentCourseFromGrades(long studentIndex, List<Course> studentCourses, Course currrentCourse) {
        for(Grade grade : currrentCourse.getGrades()) {
            if(grade.getStudent().getIndex() == studentIndex) {
                studentCourses.add(currrentCourse);
                break;
            }
        }
    }
}
