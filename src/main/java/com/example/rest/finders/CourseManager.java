package com.example.rest.finders;

import com.example.rest.models.Course;
import com.example.rest.models.Grade;
import com.example.rest.utils.DirectionEnum;
import com.example.rest.utils.GradesListUtil;
import org.mongodb.morphia.Datastore;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CourseManager {

    private Datastore datastore;

    public CourseManager(Datastore datastore) {
        this.datastore = datastore;
    }

    public List<Course> findCourses(String lecturer, String name) {
        List<Course> courses = datastore.find(Course.class).asList();
        if(lecturer != null && !lecturer.isEmpty()) {
            courses = courses.stream().filter(course -> course.getLecturer().equals(lecturer)).collect(Collectors.toList());
        }

        if(name != null && !name.isEmpty()) {
            courses = courses.stream().filter(course -> course.getName().toLowerCase()
                    .equals(name.toLowerCase())).collect(Collectors.toList());
        }

        return courses;
    }

    public Course findCourseById(long courseId) {
        Course result = datastore.find(Course.class).field("courseId").equal(courseId).get();
        return result;
    }

    public Course saveCourse(Course course) {
        course.initializeCourseId();
        datastore.save(course);
        return course;
    }

    public Course updateCourse(long id, Course newData) {
        Course updatedCourse = findCourseById(id);

        if(updatedCourse == null) {
            return null;
        } else {
            newData.setId(updatedCourse.getId());
            newData.setCourseId(updatedCourse.getCourseId());
            newData.setGrades(updatedCourse.getGrades());
        }

        datastore.save(newData);

        return newData;
    }

    public boolean deleteCourse(long id) {
        Course toDelete = findCourseById(id);

        if(toDelete == null)
            return false;

        datastore.delete(toDelete);
        return true;
    }

    public List<Grade> findCourseGrades(long id, Double note, DirectionEnum noteDir, Date date, DirectionEnum dateDir) {
        Course course = findCourseById(id);

        if(course == null)
            return null;

        List<Grade> grades = course.getGrades();
        if(note != null) {
            grades = GradesListUtil.getGradesByNote(grades, note, noteDir);
        }

        if(date != null) {
            grades = GradesListUtil.getGradesByDate(grades, date, dateDir);
        }

        return grades;
    }
}
