package com.example.rest.resources;

import com.example.rest.finders.GradeManager;
import com.example.rest.finders.StudentManager;
import com.example.rest.models.Grade;
import com.example.rest.utils.WSIDatastore;
import com.example.rest.models.Course;
import com.example.rest.models.Student;
import com.example.rest.utils.DirectionEnum;
import com.mongodb.DuplicateKeyException;
import org.mongodb.morphia.Datastore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.Date;
import java.util.List;

/**
 * @author Filip Winiarz
 */
@Path("/students")
public class StudentResource {

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Student> getStudents(@QueryParam("firstname") String firstName,
                                     @QueryParam("lastname") String lastName,
                                     @QueryParam("direction") @DefaultValue("EQUAL") DirectionEnum direction,
                                     @QueryParam("birthdate") Date date,
                                     @QueryParam("indexQuery") Long indexQuery) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        List<Student> students = new StudentManager(datastore).findStudents(firstName, lastName, date, direction, indexQuery);

        return students;
    }

    @Path("/{index}")
    @GET
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getStudent(@PathParam("index") final Long index) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        Student student = new StudentManager(datastore).findStudentByIndex(index);

        if(student == null) {
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());
        }

        return Response.ok(student).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createStudent(@NotNull @Valid Student student, @Context UriInfo uriInfo) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();

        try {
            student = new StudentManager(datastore).saveStudent(student);
        } catch (DuplicateKeyException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Duplicate student index!").build();
        }

        URI uri = uriInfo.getAbsolutePathBuilder().path(Long.toString(student.getIndex())).build();

        return Response.created(uri).entity(student).build();
    }

    @Path("/{index}")
    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateStudent(@PathParam("index") final Long index, Student newData) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        Student updatedStudent = new StudentManager(datastore).updateStudent(index, newData);

        if(updatedStudent == null)
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());

        return Response.ok(newData).build();
    }

    @Path("/{index}")
    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteStudent(@PathParam("index") final long index) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        boolean deleted = new StudentManager(datastore).cascadeDeleteStudent(index);

        if(!deleted)
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());

        return Response.ok("Student with index " + index + " removed").build();
    }

    @Path("/{index}/grades")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Grade> getStudentGrades(@PathParam("index") final long index,
                                 @QueryParam("note") Double note,
                                 @QueryParam("noteDirection") @DefaultValue("EQUAL") DirectionEnum noteDirection,
                                 @QueryParam("date") Date date,
                                 @QueryParam("dateDirection") @DefaultValue("EQUAL") DirectionEnum dateDirection,
                                 @QueryParam("courseId") Long courseId) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        List<Grade> grades = new StudentManager(datastore).findStudentGrades(index, note, courseId, noteDirection, date, dateDirection);

        return grades;
    }

    @Path("/{index}/grades")
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createGrade(@PathParam("index") long studentIndex, @NotNull @Valid Grade grade, @Context UriInfo uriInfo) {
        if(grade == null || !grade.validateNote()) {
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity("Invalid grade").build());
        }

        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        Grade savedGrade = new GradeManager(datastore).saveGrade(studentIndex, grade);

        if(savedGrade == null)
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());

        URI uri = uriInfo.getAbsolutePathBuilder().path(Long.toString(grade.getId())).build();
        return Response.created(uri).entity(grade).build();
    }

    @Path("/{index}/grades/{id}")
    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateGrade(@PathParam("index") long studentIndex,
                                @PathParam("id") long gradeId,
                                @NotNull @Valid Grade grade) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        Grade updatedGrade = new GradeManager(datastore).updateGrade(studentIndex, gradeId, grade);

        if(updatedGrade == null)
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());

        return Response.ok(updatedGrade).build();
    }

    @Path("/{index}/courses")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Course> getStudentCourses(@PathParam("index") final long index) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        List<Course> studentCourses = new StudentManager(datastore).findStudentCourses(index);
        return studentCourses;
    }
}
