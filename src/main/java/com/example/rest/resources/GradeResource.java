package com.example.rest.resources;

import com.example.rest.finders.GradeManager;
import com.example.rest.finders.StudentManager;
import com.example.rest.utils.DirectionEnum;
import com.example.rest.utils.WSIDatastore;
import com.example.rest.models.Course;
import com.example.rest.models.Grade;
import com.example.rest.models.Student;
import com.example.rest.utils.GradesListUtil;
import org.mongodb.morphia.Datastore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author Filip Winiarz
 */
@Path("/grades")
public class GradeResource {

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Grade> getGrades(@QueryParam("note") Double note,
                                 @QueryParam("noteDirection") @DefaultValue("EQUAL") DirectionEnum noteDirection,
                                 @QueryParam("date") Date date,
                                 @QueryParam("dateDirection") @DefaultValue("EQUAL") DirectionEnum dateDirection,
                                 @QueryParam("studentIndex") Long studentIndex,
                                 @QueryParam("courseId") Long courseId) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        List<Grade> grades = new GradeManager(datastore).findGrades(note, noteDirection, date, dateDirection, studentIndex, courseId);

        return grades;
    }

    @Path("/{id}")
    @GET
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getGrade(@PathParam("id") final long id) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        Grade grade = new GradeManager(datastore).findGradeById(id);

        if(grade == null)
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());

        return Response.ok(grade).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createGrade(@NotNull @Valid Grade grade, @Context UriInfo uriInfo) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();

        if(grade.getStudent() == null)
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Student not found").build());

        Grade savedGrade = new GradeManager(datastore).saveGrade(grade.getStudent().getIndex(), grade);

        if(savedGrade == null)
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());

        URI uri = uriInfo.getAbsolutePathBuilder().path(Long.toString(savedGrade.getId())).build();
        return Response.created(uri).entity(savedGrade).build();
    }

    @Path("/{id}")
    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateGrade(@PathParam("id") final long id, @NotNull @Valid Grade newData) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();

        if(newData.getStudent() == null)
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Student not found").build());

        Grade updatedGrade = new GradeManager(datastore).updateGrade(newData.getStudent().getIndex(), id, newData);

        if(updatedGrade == null) {
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());
        }

        return Response.ok(newData).build();
    }

    @Path("/{id}")
    @DELETE
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response deleteGrade(@PathParam("id") final long id) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        boolean deleted = new GradeManager(datastore).deleteGrade(id);

        if(!deleted) {
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());
        }

        return Response.ok("Grade with index " + id + " removed").build();
    }
}
