package com.example.rest.resources;

import com.example.rest.models.Counter;
import com.example.rest.models.Course;
import com.example.rest.models.Grade;
import com.example.rest.models.Student;
import com.example.rest.utils.InitialData;
import com.example.rest.utils.WSIDatastore;
import org.mongodb.morphia.Datastore;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.List;

@Path("/config")
public class ConfigResource {

    @Path("/reinit")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response reinitDatabase() {
        Datastore ds = WSIDatastore.getInstance().getDatastore();
        ds.find(Student.class).asList().forEach(ds::delete);
        ds.find(Course.class).asList().forEach(ds::delete);
        ds.find(Grade.class).asList().forEach(ds::delete);
        ds.find(Counter.class).asList().forEach(ds::delete);

        try {
            InitialData.initializeData();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.ok("Database successfully rebuilt").build();
    }

}
