package com.example.rest.resources;

import com.example.rest.finders.CourseManager;
import com.example.rest.models.Grade;
import com.example.rest.utils.DirectionEnum;
import com.example.rest.utils.WSIDatastore;
import com.example.rest.models.Course;
import com.example.rest.utils.GradesListUtil;
import org.mongodb.morphia.Datastore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Date;
import java.util.List;

/**
 * @author Filip Winiarz
 */
@Path("/courses")
public class CourseResource {
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Course> getCourses(@QueryParam("lecturer") String lecturer,
                                   @QueryParam("name") String name) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        List<Course> courses = new CourseManager(datastore).findCourses(lecturer, name);
        return courses;
    }

    @Path("/{courseId}")
    @GET
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getCourse(@PathParam("courseId") final long id) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        Course course = new CourseManager(datastore).findCourseById(id);

        if(course == null) {
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());
        }

        return Response.ok(course).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createCourse(@NotNull @Valid Course course, @Context UriInfo uriInfo) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        Course savedCourse = new CourseManager(datastore).saveCourse(course);
        URI uri = uriInfo.getAbsolutePathBuilder().path(Long.toString(savedCourse.getCourseId())).build();
        return Response.created(uri).entity(savedCourse).build();
    }

    @Path("/{id}")
    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateCourse(@PathParam("id") final long id, @NotNull @Valid Course newData) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        Course updatedCourse = new CourseManager(datastore).updateCourse(id, newData);

        if(updatedCourse == null) {
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());
        }

        return Response.ok(newData).build();
    }

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteCourse(@PathParam("id") final long id) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        boolean deleted = new CourseManager(datastore).deleteCourse(id);

        if(!deleted) {
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());
        }

        return Response.ok("Course with index " + id + " removed").build();
    }

    @Path("/{id}/grades")
    @GET
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Grade> getCourseGrades(@PathParam("id") final long id,
                                 @QueryParam("note") Double note,
                                 @QueryParam("noteDirection") @DefaultValue("EQUAL") DirectionEnum noteDirection,
                                 @QueryParam("date") Date date,
                                 @QueryParam("dateDirection") @DefaultValue("EQUAL") DirectionEnum dateDirection) {
        Datastore datastore = WSIDatastore.getInstance().getDatastore();
        List<Grade> grades = new CourseManager(datastore).findCourseGrades(id, note, noteDirection, date, dateDirection);

        if(grades == null) {
            throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("Not found").build());
        }

        return grades;
    }
}
